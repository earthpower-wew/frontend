/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation } from '@angular/core';

import { NavbarComponent } from './core/navbar/navbar.component';
import { SideNavComponent } from './core/sidenav/sidenav.component';
import { FooterComponent } from './core/footer/footer.component';


@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  directives: [NavbarComponent, SideNavComponent, FooterComponent],
  styleUrls: [
    './app.style.css'
  ],
  template: `
    <header>
      <app-core-nav [routes]="routes" [defaultRoute]="defaultRoute"></app-core-nav>
      <app-core-sidenav [svgLogo]="logo"></app-core-sidenav>
    </header>
    <main>
      <div class="container">
        <div class="row">
          <router-outlet></router-outlet>
        </div>
      </div>
    </main>
    <app-core-footer></app-core-footer>
  `
})
export class App {
  routes = [
    "home",
    "about"
  ];

  defaultRoute = "home";

  logo = 'assets/img/logo.svg';
}
