import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-core-nav',
  styleUrls: ['navbar.css'],
  template: `
      <nav class="top-nav navbar-fixed">
        <div class="navbar-wrapper container">
          <div class="left hide-on-med-and-down">
            <a *ngFor="let route of routes" [class.active]="isActive(route)" [routerLink]="root(route)" class="btn">
              {{route}}
            </a>
          </div>
        </div>
      </nav>
  `
})
export class NavbarComponent {
  @Input() routes: Array<String>;
  @Input() defaultRoute: String = "";


  constructor(private location: Location) {
  }

  isActive(route: string): boolean {
    return this.location.isCurrentPathEqualTo(this.root(route)) ||
      (this.location.isCurrentPathEqualTo("") && route == this.defaultRoute);
  }

  private root(route: string): string {
    return "/" + route;
  }
}
