import { Component, Input } from '@angular/core';
import { SvgComponent } from '../../util/svg/svg.component';
import { SearchComponent } from '../../util/search/search.component';

@Component({
  selector: 'app-core-sidenav',
  directives: [SvgComponent, SearchComponent],
  template: `
    <div class="side-nav fixed right">
      <app-svg class="brand-logo" [src]="svgLogo"></app-svg>
      <app-search></app-search>
    </div>
  `
})
export class SideNavComponent {
  svgLogo = "assets/img/logo.svg";
}
