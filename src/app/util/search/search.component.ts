import { Component, Input, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizationService } from '@angular/platform-browser';

@Component({
  selector: 'app-search',
  styleUrls: ['search.css'],
  template: `
    <div class="search search-wrapper card">
      <span class="fa fa-search"></span>
      <input #userInput (keyup)="0" placeholder="Search term">
      <div class="search-results card">{{userInput.value}}</div>
    </div>
  `
})
export class SearchComponent{
  //todo;
}
