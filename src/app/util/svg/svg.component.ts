import { Component, Input, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizationService } from '@angular/platform-browser';

@Component({
  selector: 'app-svg',
  styles: [`
    object {
      width: 100%;
      padding: inherit;
    }
  `],
  template: `
    <object type="image/svg+xml" [data]="url">Your browser does not support svg images.</object>
  `
})
export class SvgComponent implements OnInit {
  @Input() src: string;

  private searchData: string;

  private url: SafeResourceUrl;

  constructor(private sanitizer: DomSanitizationService) {}

  ngOnInit() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.src);
  }
}
